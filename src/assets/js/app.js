/**
 * Závislosti
 */

import $ from 'jquery';
import 'what-input';

// jQuery
window.jQuery = $;

// Celý Foundation
require('foundation-sites');

// Jen vybrané části z Foundationu
// import './lib/foundation-explicit-pieces';

// Lazyloading
import LazyLoad from "vanilla-lazyload";

// Youtubí funkce
import "async-youtube-video";

// Animační knihovna
import AOS from "aos";

// Vyskakovací obrázky
import "./lib/simplelightbox";

/**
 * Inicializace
 */

// Lazyloading images
let lazy = new LazyLoad({
    elements_selector: ".lazy"
});

// Animate On Scroll
let aos = AOS.init();

// Readmore functionality
$(document).ready(function(){
    $(".readmore-toggle").on("click",function(e){
        e.preventDefault();
        let targetID = $(this).attr("data-target");
        if (targetID) {
            let target = $(targetID);
            if ( target ) {
                let state = target.attr("data-readmore");
                if (state=="collapsed") {
                    target.attr("data-readmore","expanded");
                    $(this).html( $(this).attr("data-expanded-text") );
                } else {
                    target.attr("data-readmore","collapsed");
                    $(this).html( $(this).attr("data-collapsed-text") );
                }
            }
        }
    });
});



$(document).foundation();

/**
 * Vlastní kód
 */

$(document).ready(function(){
    let projects = $(".project-trigger");
    let projectsContainer = $("#projects");

    projects.on("click",function(){

        let thisIndex = $(this).attr("data-index");

        if ( thisIndex != undefined ) {
            if ( projectsContainer.attr("data-active") != thisIndex ) {
                projectsContainer.attr("data-active",thisIndex);
            } else {
                projectsContainer.attr("data-active","");
            }
        }

    });

    let people = $(".person-trigger");
    let peopleContainer = $("#people");

    people.on("click",function(){

        let thisIndex = $(this).attr("data-index");

        if ( thisIndex != undefined ) {
            if ( peopleContainer.attr("data-active") != thisIndex ) {
                peopleContainer.attr("data-active",thisIndex);
            } else {
                peopleContainer.attr("data-active","");
            }
        }

    });

    let events = $(".events-trigger");
    let eventsContainer = $("#events");

    events.on("click",function(){

        let thisIndex = $(this).attr("data-index");

        if ( thisIndex != undefined ) {
            if ( eventsContainer.attr("data-active") != thisIndex ) {
                eventsContainer.attr("data-active",thisIndex);
            } else {
                eventsContainer.attr("data-active","");
            }
        }

    });

    $(".deactivator").on("click",function(){
        $(this).closest(".expandable-holder").attr("data-active","");
    });

});


/**
 * Youtubí videa asynchronně
 */

// všechny videa vložená do HTML pomocí datasetů
let youtube = document.querySelectorAll(".youtube");

// kontejner pro vykreslení videjí z datasetů
let embeds = []; 

// projet všechny elementy s datasetem a tvorba pole pro vložení
for (let i = 0; i < youtube.length; i++) {

    let thumbnail = "https://img.youtube.com/vi/" + youtube[i].dataset.embed+"/sddefault.jpg";
    let preload = false;
    if (youtube[i].dataset.preload == "true") {
        preload = true;
    }
    let target = $( youtube[i] ).addClass( "yt-" + youtube[i].dataset.embed );
    if ( youtube[i].dataset.target != undefined) {
        target = youtube[i].dataset.target;
    }

    let videoId = youtube[i].dataset.embed;
    
    let videoObject = {
        videoId: videoId,
        target: target,
        thumbnail: thumbnail, 
        preload: preload
    };
    embeds.push( videoObject );

}

// vložit videa z připraveného pole
window.onload = function() {

    console.log( "window onload" );

    for ( let i = 0; i < embeds.length; i++ ) {
        let embed = embeds[i];

        asyncYoutubeVideo.init({
            targetClass: embed.target,
            videoId: embed.videoId,
            controls: 1,
            autoplay: 0,
            disablekb: 1,
            fs: 0,
            modestbranding: 1,
            playsinline: 1,
            showinfo: 0,
            origin: 'alveda.cz',
            additionalClasses: 'test-video some-class',
            rel: 0,
            mute: 1,
            loop: 1
          });

    }
    
  }

/**
 * Sosnout videa z kanálu
 */
const apik = "AIzaSyD2x70-rjvNL0RuEePC6YHU_dp2YFsCk00"; 
const channelId = "UCVbd5PwCxJ46Eohjne8ci6A"; 
const skipVideo = "5TUoeAmi624";
let channelVideos = [];
let videoThumbs = [];

/*
$(document).ready(function() {
    $.get(
      "https://www.googleapis.com/youtube/v3/search",{
        part : 'snippet', 
        channelId : channelId,
        type : 'video',
        key: apik,
        maxResults: 50,
        order: "date",
        videoEmbeddable: true, 
        },
        function(data) {
            console.log("Data");
            console.log(data);
            // Zformátovat data do správné podoby
            $.each( data.items, function( i, item ) {
                
                // Přeskočit video, které je už zobrazené nahoře
                if (item.id.videoId != skipVideo) {
                    // console.log(item);
                    let videoItem = {
                        title: item.snippet.title,
                        id: item.id.videoId,
                        img: item.snippet.thumbnails.default.url,
                        srcset: prepareYoutubeSrcset(item),
                    };
                    channelVideos.push(videoItem);
                }
                
            });
            // Jakmile jsou zformátovaná, vytisknout celý obsah
            for ( let i = 0; i < channelVideos.length; i++) {
                let simpleItem = channelVideos[i];
                // první video se vykreslí velké
                if (i==0) {
                    playVideo(simpleItem.id);
                    renderVideoThumbnail(simpleItem, 1);
                } 
                // ostatní videa se vykreslí malá
                else  {
                    console.log(simpleItem);
                    renderVideoThumbnail(simpleItem);
                }
            }

           $("#video-list").find(".video-trigger").each(function(){
               $(this).on("click",function(){

                    // zkontrolovat jestli jsem neklikl na aktivní element
                    if ( !$(this).hasClass("active") ) {
                        // přehrát video
                        let play = $(this).attr("data-play");
                        playVideo(play,true);
                        // deaktivovat aktuální aktivní prvek
                        $("#video-list").find(".video-trigger.active").each(function(){
                            $(this).removeClass("active");
                        });
                        //aktivovat nový aktivní prvek
                        $(this).addClass("active");
                        // Odscrollovat nahorů na video
                        let playerPos = $("#video-main").offset().top - 80;
                        console.log(playerPos);
                        // $(window).scrollTop(playerPos);
                        var body = $("html, body");
                        body.stop().animate({scrollTop:playerPos}, 500, 'swing');
                    }

               });

           });

        }
    );
});

function playVideo(videoId, auto=false){

    if (auto) {
        let video = document.getElementById("video-main");
        video.innerHTML = "";
        let container = document.createElement("div");
        container.setAttribute("class","main-video-player");
        video.appendChild(container);
    }

    asyncYoutubeVideo.init({
        targetClass: "main-video-player",
        videoId: videoId,
        controls: 1,
        autoplay: auto,
        disablekb: 1,
        fs: 1,
        modestbranding: 0,
        playsinline: 1,
        showinfo: 0,
        origin: 'alveda.cz',
        additionalClasses: 'test-video some-class',
        rel: 0,
        mute: 0,
        loop: 0
      });

}

function renderVideoThumbnail(vid, active=false){
    
    // Vytvořit container
    let element = document.createElement("div");
    element.setAttribute("id","ytv-"+vid.id);
    element.setAttribute("data-play", vid.id);
    // element.setAttribute("class","padding-half");

    let commonClasses = "video-trigger columns small-6 medium-4 large-3 xlarge-2";
    
    if (active) {
        element.setAttribute("class", commonClasses + " active" );
    } else {
        element.setAttribute("class", commonClasses );
    }

    // Yytvořit obrázek
    let image = document.createElement("img");
    image.setAttribute("src",vid.img);
    image.setAttribute("class","lazy youtube-video-thumbnail");
    image.setAttribute("srcset",vid.srcset);
    element.appendChild(image);

    // Vytvořit popisku
    let desc = document.createElement("div");
    desc.setAttribute("class","youtube-video-name");
    let title = document.createElement("div");
    title.setAttribute("class","padding-quarter");
    title.innerHTML = vid.title;
    desc.appendChild(title);
    element.appendChild(desc);

    // Nakonec vložit celý element do DOM
    document.getElementById("video-list").appendChild(element);
}

function prepareYoutubeSrcset(vid){
    let o = vid.snippet.thumbnails.default.url + " 120w";
    o += ", " + vid.snippet.thumbnails.medium.url + " 320w";
    o += ", " + vid.snippet.thumbnails.high.url + " 480w";
    return o;
}

*/

// spustit lightbox
let lightbox;
$(document).ready(function(){
    let options = {};
    
    lightbox = $('#galerie a').simpleLightbox(options);

});


// manuální load
$(document).ready(function(){

    $(".manual-lazyload").on("click",function(){
        let target = $(this).attr("data-manual-lazyload");
        // console.log(target);
        load_all_content($("#"+target));
    });

});


// Manuální spouštění lazyloadu
function load_all_content(element) {
    let lazyContent = $(element).find(".lazy");
    if (lazyContent.length > 0) {
        for (let i = 0; i < lazyContent.length; i++) {
            let item = lazyContent[i];
            // console.log(item);
            lazy.load(item);
        }
    }
}

// Mobilní navigace

let titlebar = $("#titlebar");
let titlebarState = titlebar.attr("data-state");
let titlebarTrigger = $("#pseudoTrigger");
let hamburger = $("#hamburger");

function switchTitlebarState(){
    if (titlebarState == "collapsed") {
        setTitlebarState("expanded");
        hamburger.removeClass("menu-icon").addClass("x-button").html("x");
    } else {
        setTimeout(function() {
            setTitlebarState("collapsed");
        }, 200);
        
        hamburger.removeClass("x-button").addClass("menu-icon").html("");
    }
}

function setTitlebarState( state ) {
    titlebar.attr("data-state",state);
    titlebarState = state;
}
$(document).ready(function(){
    titlebarTrigger.on("click",function(){
        switchTitlebarState();
    });

    titlebar.find("a").on("click",function(){
        switchTitlebarState();
    });


    let longestHeight = 0;
    setProjectsHeight( longestHeight );
    $(window).resize(function(){
        longestHeight = getLongestProject();
        setProjectsHeight(longestHeight);
    });
});

function getLongestProject() {
    let projectsForHeight = $(".project-detail");
    let max = 0;
    projectsForHeight.each(function(){
        let h = $(this).innerHeight();
        if ( h > max ) {
            max = h;
        }
    });
    // console.log(max);
    return max;
}

function setProjectsHeight(h){
    $("#projects").css("min-height",h);
}

$("#o_nas").on("click",function(){
    scrollToIntro();
});

$("#arrow_down").on("click",function(){
    scrollToIntro();
});

function scrollToIntro(){
    let offset = $("#presentation").offset();
    $("html, body").stop().animate({scrollTop:offset.top - 80}, 500, 'swing');
};







